import numpy as np
import abc
class ActivationFunction(abc.ABC):
    @abc.abstractmethod
    def f(self, x):
        pass

    @abc.abstractmethod
    def f_prim(self, x):
        pass

class TanhFunction(ActivationFunction):
    def f(self, x):
        return np.tanh(x)

    def f_prim(self, x):
        return 1.0 - np.tanh(x) ** 2

class SigmoidFunction(ActivationFunction):
    def f(self, x):
        exp = np.exp(x)
        return exp / (exp + 1)

    def f_prim(self, x):
        exp = np.exp(x)
        return exp / (exp + 1)**2

class ReLUFunction(ActivationFunction):
    def f(self, x):
        return np.maximum(np.zeros(len(x)), x)

    def f_prim(self, x):
        print(x)
        d = [1 if (s > 0) else 0 for s in x[0]]
        return np.array(d)

class X(ActivationFunction):
    def f(self, x):
        return x
    def f_prim(self, x):
        return 1

class SinFunction(ActivationFunction):
    def f(self, x):
        return np.sin(x)

    def f_prim(self, x):
        return np.cos(x)

class Marioid(ActivationFunction):
    def f(self, x):
        beta = -1
        return 1/(1 + np.exp((beta * x)))

    def f_prim(self, x):
        f = self.f(x)
        return f * (1 - f)