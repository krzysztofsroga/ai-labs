class AnimalStats:
    def __init__(self, name, weights, heights):
        self.name = name
        self.weights = weights
        self.heights = heights


class AnimalGenData:
    def __init__(self, name: str, minMean: float, minVariance: float, maxMean: float, maxVariance: float, numberOfModes: int):
        self.name = name
        self.minMean = minMean
        self.minVariance = minVariance
        self.maxMean = maxMean
        self.maxVariance = maxVariance
        self.numberOfModes = numberOfModes