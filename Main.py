from AnimalStats import AnimalStats, AnimalGenData
from GUI import CanvasPanel
from Neuron import LastNeuron, Neuron
from typing import List
import numpy as np
import random
import ActivationFunction

def generateData(animalGenData: List[AnimalGenData], numberOfSamples) -> List[AnimalStats]:
    animalList = []
    for animalGenDatum in animalGenData:
        animalWeight = np.empty(0, float)
        animalHeight = np.empty(0, float)
        for mode in range(animalGenDatum.numberOfModes):
            modeHeightMean = np.random.uniform(animalGenDatum.minMean, animalGenDatum.maxMean)
            modeHeightVariance = np.random.uniform(animalGenDatum.minVariance, animalGenDatum.maxVariance)
            modeWeightMean = np.random.uniform(animalGenDatum.minMean, animalGenDatum.maxMean)
            modeWeightVariance = np.random.uniform(animalGenDatum.minVariance, animalGenDatum.maxVariance)
            animalWeight = np.concatenate((animalWeight, np.random.normal(modeWeightMean, np.sqrt(modeWeightVariance), numberOfSamples)))
            animalHeight = np.concatenate((animalHeight, np.random.normal(modeHeightMean, np.sqrt(modeHeightVariance), numberOfSamples)))
        animalStats = AnimalStats(animalGenDatum.name, weights=animalWeight, heights=animalHeight)
        animalList.append(animalStats)

    allData = []
    allLabels = []
    for a in animalList:
        allData += zip(a.weights, a.heights)
        tmp = 1 if a.name == 'Dogs' else 0
        allLabels += [[tmp] for x in range(len(a.heights))]
    tmp1 = list(zip(allData, allLabels))
    random.shuffle(tmp1)
    print(tmp1)
    allData, allLabels = zip(*tmp1)
    print(allData)
    print(allLabels)
    # print(allData)
    # print(allLabels)
    print("Training started!")
    # neuron = LastNeuron(2, ActivationFunction.Marioid())
    neuron = Neuron(2, ActivationFunction.Marioid())
    # neuron = LastNeuron(2, ActivationFunction.ReLUFunction())
    line = neuron.train(allData, allLabels, 1000)
    print("Training finished!")

    predictedCats = AnimalStats("Predicted Cats", [], [])
    predictedDogs = AnimalStats("Predicted Dogs", [], [])

    debugText = []

    for weight in range(-50, 50, 2):
        for height in range(-50, 50, 2):
            coord = (1, weight, height)
            # prediction = neuron.f(neuron.predict(np.array(coord)))
            prediction = neuron.predict(np.array(coord))
            debugText.append(prediction)
            if prediction >= 0.5:
                predictedCats.weights.append(weight)
                predictedCats.heights.append(height)
            else:
                predictedDogs.weights.append(weight)
                predictedDogs.heights.append(height)

    # print(debugText)
    animalList.append(predictedDogs)
    animalList.append(predictedCats)
    return animalList


def guiCallback(numberOfSamples, animals):
    panel.setData(generateData(animals, numberOfSamples=numberOfSamples))

panel = CanvasPanel(guiCallback)
# neuron = Neuron()


if __name__ == "__main__":
    # sample = np.array([[1, 1], [1, 0], [0, 1], [0, 0]])
    # labels = np.array([[1, 1, 1, 0]]).T
    # neuron.train(sample, labels, 1000)
    # print(neuron.predict(np.array([0, 0])))
    # print(neuron.predict(np.array([0, 1])))
    # print(neuron.predict(np.array([1, 0])))
    # print(neuron.predict(np.array([1, 1])))
    #
    # a = [
    #     AnimalGenData("Dogs", 25, 9, 20, 16),
    #     AnimalGenData("Cats", 20, 4, 10, 9)
    # ]
    # panel.setData(generateData(a, numberOfSamples=100))

    panel.mainLoop()
