import matplotlib
import wx
import numpy as np
from AnimalStats import AnimalStats, AnimalGenData
from typing import List

matplotlib.use('WXAgg')

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
from matplotlib import pyplot as plt


class CanvasPanel(wx.Panel):
    def __init__(self, submitCallback):
        self.submitCallback = submitCallback
        self.app = wx.App()
        self.fr = wx.Frame(None, title='Statistics of Animals', size=wx.Size(600, 600))

        wx.Panel.__init__(self, self.fr)
        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)

        labelSamples = wx.StaticText(self, -1, "Samples/mode: ")
        self.inputSamples = wx.TextCtrl(self, -1, "50")
        labelModes = wx.StaticText(self, -1, "Modes/type: ")
        self.inputModes = wx.TextCtrl(self, -1, "1")
        labelIterations = wx.StaticText(self, -1, "Iterations")
        self.inputIterations = wx.TextCtrl(self, -1, "1000")
        self.submitButton = wx.Button(self, -1, "Submit")
        self.submitButton.Bind(wx.EVT_BUTTON, self.onSubmit)

        labelCatMinMean = wx.StaticText(self, -1, "Cat min mean: ")
        labelCatMinVariance = wx.StaticText(self, -1, "Cat min variance: ")
        labelCatMaxMean = wx.StaticText(self, -1, "Cat max mean: ")
        labelCatMaxVariance = wx.StaticText(self, -1, "Cat max variance: ")
        self.inputCatMinMean = wx.TextCtrl(self, -1, "-50")
        self.inputCatMinVariance = wx.TextCtrl(self, -1, "1")
        self.inputCatMaxMean = wx.TextCtrl(self, -1, "50")
        self.inputCatMaxVariance = wx.TextCtrl(self, -1, "4")

        labelDogMinMean = wx.StaticText(self, -1, "Dog min mean: ")
        labelDogMinVariance = wx.StaticText(self, -1, "Dog min variance: ")
        labelDogMaxMean = wx.StaticText(self, -1, "Dog max mean: ")
        labelDogMaxVariance = wx.StaticText(self, -1, "Dog max variance: ")
        self.inputDogMinMean = wx.TextCtrl(self, -1, "0")
        self.inputDogMinVariance = wx.TextCtrl(self, -1, "2")
        self.inputDogMaxMean = wx.TextCtrl(self, -1, "30")
        self.inputDogMaxVariance = wx.TextCtrl(self, -1, "9")

        self.canvas = FigureCanvas(self, -1, self.figure)


        animalsSizer = wx.GridSizer(rows=4, cols=4, hgap=5, vgap=5)
        buttonsSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        tSizer1 = wx.BoxSizer(wx.HORIZONTAL)
        tSizer2 = wx.BoxSizer(wx.HORIZONTAL)


        animalsSizer.Add(labelCatMinMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputCatMinMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelCatMinVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputCatMinVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelCatMaxMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputCatMaxMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelCatMaxVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputCatMaxVariance, 1, wx.LEFT, wx.CENTER)

        animalsSizer.Add(labelDogMinMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputDogMinMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelDogMinVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputDogMinVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelDogMaxMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputDogMaxMean, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(labelDogMaxVariance, 1, wx.LEFT, wx.CENTER)
        animalsSizer.Add(self.inputDogMaxVariance, 1, wx.LEFT, wx.CENTER)

        tSizer1.Add(labelSamples, 1, wx.LEFT | wx.CENTER)
        tSizer1.Add(self.inputSamples, 1, wx.LEFT | wx.TOP)
        tSizer1.Add(labelModes, 1, wx.LEFT | wx.CENTER)
        tSizer1.Add(self.inputModes, 1, wx.LEFT | wx.TOP)

        tSizer2.Add(labelIterations, 1, wx.LEFT | wx.CENTER)
        tSizer2.Add(self.inputIterations, 1, wx.LEFT | wx.TOP)
        tSizer2.Add(self.submitButton, 1, wx.LEFT | wx.TOP)

        buttonsSizer.Add(tSizer1, 1, wx.LEFT | wx.CENTER)
        buttonsSizer.Add(tSizer2, 1, wx.LEFT | wx.CENTER)

        mainSizer.Add(buttonsSizer, 1, wx.CentreX | wx.TOP)
        mainSizer.Add(animalsSizer, 1, wx.CentreX | wx.TOP)
        mainSizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)

        self.SetSizer(mainSizer)
        self.Fit()

    def onSubmit(self, event):
        cats = AnimalGenData("Cats",
                             float(self.inputCatMinMean.GetValue()),
                             float(self.inputCatMinVariance.GetValue()),
                             float(self.inputCatMaxMean.GetValue()),
                             float(self.inputCatMaxVariance.GetValue()),
                             int(self.inputModes.GetValue()))
        dogs = AnimalGenData("Dogs",
                             float(self.inputDogMinMean.GetValue()),
                             float(self.inputDogMinVariance.GetValue()),
                             float(self.inputDogMaxMean.GetValue()),
                             float(self.inputDogMaxVariance.GetValue()),
                             int(self.inputModes.GetValue()))
        self.submitCallback(int(self.inputSamples.GetValue()), [cats, dogs])

    def setData(self, animals: List[AnimalStats]):
        self.figure.delaxes(self.axes)
        self.axes = self.figure.add_subplot(111)
        self.axes.set_title('Animals')
        self.axes.set_ylabel('weight [Kg]')
        self.axes.set_xlabel('height [cm]')
        for animal in animals:
            self.axes.scatter(animal.heights, animal.weights, s=1, marker='o', label=animal.name)
        self.axes.legend(loc='lower right', borderaxespad=0.)
        self.canvas.draw()
        self.Update()

    def mainLoop(self):
        self.fr.Show()
        self.app.MainLoop()
