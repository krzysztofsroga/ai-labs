import numpy as np
import ActivationFunction

#
# class Neuron:
#     def __init__(self, activationFunction: ActivationFunction.ActivationFunction):
#         dimensions = (2, 1)
#         self.learningRate = 0.01
#         self.weights = np.array(2 * np.random.random(dimensions) - 1)
#         self.activationFunction = activationFunction
#
#     def predict(self, input):
#         return self.activationFunction.f(np.dot(input, self.weights))
#
#     def train(self, sample, expectedLabel, iterations):
#         for i in range(iterations):
#             self.trainIteration(sample, expectedLabel)
#
#     def trainIteration(self, sample, expectedLabels):
#         for (singleSample, singleLabel) in zip(sample, expectedLabels):
#             self.trainSingleSample(singleSample, singleLabel)
#
#     def trainSingleSample(self, singleSample, singleLabel):
#         predictedLabel = self.predict(singleSample)[0]
#         a = self.learningRate * (singleLabel - predictedLabel)
#         b = self.activationFunction.f_prim(np.dot(self.weights.T, singleSample))
#         adjustment = np.dot(np.dot(a, b), singleSample)
#         adj = np.array([adjustment]).T
#         corrected = self.weights + adj
#         self.weights = corrected
#
#
# class BetterNeuron:
#     def __init__(self, activationFunction: ActivationFunction.ActivationFunction):
#         dimension = (3, 1)
#         self.learningRate = 0.1
#         self.weights = np.array(2 * np.random.random(dimension) - 1)
#         self.activationFunction = activationFunction
#
#     def f(self, x):
#         return self.activationFunction.f(x)
#
#     def f_prim(self, x):
#         return self.activationFunction.f_prim(x)
#
#     def predict(self, x, y):
#         return self.f(self.weights[0] + self.weights[1] * np.array(x) + self.weights[2] * np.array(y))
#
#     def train(self, sample, expectedLabel, iterations):
#         for i in range(iterations):
#             self.trainIteration(sample, expectedLabel)
#
#     def trainIteration(self, sample, expectedLabels):  # sample: [[30, 40], [23, 59], [24, 44]] expected labels: [1, -1, 1]
#         n = len(sample)
#         d = self.predict(*zip(*sample))
#         y = list(zip(*expectedLabels))[0]
#         x = [[1] * n] + list(zip(*sample))
#
#         d_w = [None] * 3
#         for i in range(3):
#             s = self.weights[i] * x
#             d_w_i = np.multiply(np.multiply((d - y), self.f_prim(s)), x[i])
#             d_w[i] = [np.sum(d_w_i) * self.learningRate]
#         print(self.weights)
#         print(d_w)
#         self.weights += d_w



class LastNeuron:
    def __init__(self, numOfInputs, activationFunction: ActivationFunction.ActivationFunction):
        self.numOfInputs = numOfInputs
        weightDimension = (1, numOfInputs + 1)
        self.learningRate = 0.5
        #self.weights = np.ones(numOfInputs+1)
        self.weights = np.array([0.5, 0.5, 0.5])#np.array(2 * np.random.random(weightDimension) - 1)
        self.activationFunction = activationFunction

    def f(self, x):
        return self.activationFunction.f(x)

    def f_prim(self, x):
        return self.activationFunction.f_prim(x)

    def predict(self, input): #input - 3xn array, first row is ones
        return self.weights @ input

    def train(self, rawSampleOfInputs, expectedLabels, iterations):
        print("Raw sample of inputs length: ", len(rawSampleOfInputs))
        sampleOfInputs = np.insert(np.array(rawSampleOfInputs).T, 0, np.ones(len(rawSampleOfInputs)), axis=0) #EWENTUALNIE MIERZENIE DRUGIEJ WIELKOŚCI!!!
        print(np.array(rawSampleOfInputs).shape)
        print(sampleOfInputs.shape)
        for i in range(iterations):
            self.trainIteration(sampleOfInputs, np.array(expectedLabels).T)

    def trainIteration(self, sampleOfInputs, expectedLabels): #returns error
        # print(sampleOfInputs)
        p = self.predict(sampleOfInputs)
        print(p.shape)

        # predictions = self.f(p)
        predictions = self.f(p)
        derivatives = self.f_prim(p)

        # print("predictions: ", predictions)
        diff = expectedLabels - predictions
        diff_der = diff * derivatives
        print(diff)
        print(diff_der)
        d_w = [None] * (self.numOfInputs + 1)
        for i in range(self.numOfInputs + 1):
            a = np.sum(diff * derivatives * sampleOfInputs[i]) * self.learningRate
            d_w[i] = a
        # print("dw: ", d_w)
        self.weights += d_w
        # print("weightsafter", self.weights)
        error = np.sum(np.abs(diff))
        print("Error: ", error)


class Neuron():

    def __init__(self, numOfInputs, activationFunction: ActivationFunction.ActivationFunction):
        self.numOfInputs = numOfInputs + 1
        self.activationFunction = activationFunction
        self.weights = 2 * np.random.random((self.numOfInputs, 1)) - 1

    def f(self, x):
        return self.activationFunction.f(x)

    def f_prim(self, x):
        return self.activationFunction.f_prim(x)

    def predict(self, inputs):
        return self.f(inputs @ self.weights)

    def train(self, sampleOfInputs, expectedLabels, iterations):
        sampleOfInputs = np.insert(np.array(sampleOfInputs).T, 0, np.ones(len(sampleOfInputs)), axis=0).T
        for iteration in range(iterations):
            predictions = self.predict(sampleOfInputs)
            adjustment = np.dot(sampleOfInputs.T, (expectedLabels - predictions) * self.f_prim(predictions))
            self.weights += adjustment

        boundary_x = np.linspace(np.amin(sampleOfInputs[1]), np.amax(sampleOfInputs[1]))
        boundary_y = -(self.weights[0] + self.weights[1] * boundary_x)/self.weights[2]
        return (boundary_x, boundary_y)